import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          actions: <Widget>[Icon(Icons.account_circle), Text('   ')],
          leading: Icon(Icons.search),
          backgroundColor: Colors.white,
          title: Text('Demo'),
          centerTitle: true,
          elevation: 0.0,
        ),
        body: Padding(
            padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
            child: Container(
                height: 400,
                child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                  new Card(
                      color: Colors.grey[400],
                      child: Container(
                          height: 400,
                          child: Column(children: <Widget>[
                        Padding(
                            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            child: Text(
                              'Test' + ' ',
                              style: TextStyle(fontSize: 23),
                            )),
                        ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                            itemCount: data[0]['data'].length,
                            itemBuilder: (BuildContext context, i) {
                              return Draggable(
                                child: Container(
                                    height: 100,
                                    width: 160,
                                    child: Card(
                                        child: Column(
                                      children: <Widget>[
                                        Text(
                                          data[0]['data'][i]['title'],
                                          style: TextStyle(fontSize: 14),
                                        ),
                                        Divider(
                                          color: Colors.white,
                                          height: 5,
                                        ),
                                        Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(12, 2, 5, 8),
                                          child: Text(
                                            data[0]['data'][i]['sub'],
                                            style: TextStyle(fontSize: 12),
                                          ),
                                        )
                                      ],
                                    ))),
                                feedback: Container(
                                    height: 100,
                                    width: 160,
                                    child: Card(
                                        child: Column(children: <Widget>[
                                      Text(
                                        'What is Lorem Ipsum?',
                                        style: TextStyle(fontSize: 14),
                                      ),
                                      Divider(
                                        color: Colors.white,
                                        height: 5,
                                      ),
                                    ]))),
                                childWhenDragging: Container(),
                              );
                            })
                      ]))),
                ]))));
  }

  List data = [
    {
      'data': [
        {
          'title': 'What is lorem ipsum',
          'sub': 'it is a long established fact that'
        },
        {
          'title': 'What is lorem ipsum',
          'sub': 'it is a long established fact that'
        },
        {
          'title': 'What is lorem ipsum',
          'sub': 'it is a long established fact that'
        },
        {
          'title': 'What is lorem ipsum',
          'sub': 'it is a long established fact that'
        }
      ]
    }
  ];
}
