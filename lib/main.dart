import 'package:flutter/material.dart';
import 'home.dart';


void main() async {
  Widget _defaultHome = Home();


  

  final routes = <String, WidgetBuilder>{
   
    
  };
  // Run app!
  runApp(MaterialApp(
    title: 'Demo_Trello',
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      primarySwatch: Colors.lightBlue,
      fontFamily: 'Nunito',
    ),
    home: _defaultHome,
    routes: routes,
  ));
}