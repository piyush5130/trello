import 'package:flutter/material.dart';

class Data {
  String title;
  String sub;

  Data(this.title, this.sub);
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<List<Data>> data = [
    [Data("hello", "world"), Data("hello", "world")],
    [
      Data("de Finibus Bonorum ", "buzz"),
      Data("facilis est et ", "et voluptates "),
      Data("Section 1.10.32 of", "Sed ut perspiciatis unde")
    ],
    [
      Data("Section 1.10.32 of", "Sed ut perspiciatis unde"),
      Data(
        "written by Cicero",
        "accusantium doloremque laudantium",
      ),
      Data("hello", "world")
    ],
    [
      Data("foo", "buzz"),
      Data("et voluptates repudiandae ", "buzz"),
    ],
  ];

  @override
  Widget build(BuildContext context) {
    // print('here here: ' + data.runtimeType.toString());
    // print('here here: ' + data[0].runtimeType.toString());
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          actions: <Widget>[Icon(Icons.account_circle), Text('   ')],
          leading: Icon(Icons.search),
          backgroundColor: Colors.white,
          title: Text('Demo'),
          centerTitle: true,
          elevation: 0.0,
        ),
        body: Container(
            height: 600,
            child: ListView(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Container(
                    child: DataColumn(data[0]),
                    width: 150,
                  ),
                  Container(
                    child: DataColumn(data[1]),
                    width: 150,
                  ),
                  Container(
                    child: DataColumn(data[2]),
                    width: 150,
                  ),
                ])));
  }
}

class DataColumn extends StatefulWidget {
  final List<Data> dataItems;

  DataColumn(this.dataItems);

  @override
  _DataColumnState createState() => _DataColumnState();
}

class _DataColumnState extends State<DataColumn> {
  void onDragComplete(int index) {
    setState(() {
      widget.dataItems.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return DragTarget(onAccept: (Data data) {
      widget.dataItems.add(data);
    }, onWillAccept: (Data data) {
      return true;
    }, builder: (context, List<Data> accepted, rejected) {
      // Data data = widget.dataItems[0];

      // return DataBox(data.title, data.sub);
      // return DataBox('data.title', 'data.sub');
      return Card(
          color: Colors.grey[400],
          child: Container(
              height: 600,
              width: MediaQuery.of(context).size.width,
              child: ListView(shrinkWrap: true, children: <Widget>[
                Center(
                    child: Padding(
                        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                        child: Text(
                          'Test' + ' ',
                          style: TextStyle(fontSize: 23),
                        ))),
                ListView.builder(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  itemCount: widget.dataItems.length ?? 0,
                  itemBuilder: (context, index) {
                    Data data = widget.dataItems[index];
                    print('hello hhhhhhhhhhhh' + data.runtimeType.toString());
                    return DataBox(data, index, this);
                  },
                )
              ])));
    });
  }
}

class DataBox extends StatelessWidget {
  final Data dataItem;
  final int index;
  final _DataColumnState parent;

  DataBox(this.dataItem, this.index, this.parent);

  @override
  Widget build(BuildContext context) {
    return Draggable(
      child: Container(
          height: 70,
          width: 100,
          child: Card(
              child: Column(
            children: <Widget>[
              Text(
                dataItem.title,
                style: TextStyle(fontSize: 14),
              ),
              Divider(
                color: Colors.white,
                height: 5,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(12, 2, 5, 8),
                child: Text(
                  dataItem.sub,
                  style: TextStyle(fontSize: 12),
                ),
              )
            ],
          ))),
      feedback: Container(
          height: 70,
          width: 140,
          child: Card(
              child: Column(
            children: <Widget>[
              Text(
                dataItem.title,
                style: TextStyle(fontSize: 14),
              ),
              Divider(
                color: Colors.white,
                height: 5,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(12, 2, 5, 8),
                child: Text(
                  dataItem.sub,
                  style: TextStyle(fontSize: 12),
                ),
              )
            ],
          ))),
      childWhenDragging: Container(),
      data: dataItem,
      onDragCompleted: () {
        parent.onDragComplete(index);
        // dataItem.title ='';
        // dataItem.sub ='';
      },
    );
  }
}

// CarouselSlider(
//     height: 600.0,
//     viewportFraction: 0.5,
//     enableInfiniteScroll: false,
//     items: [
// DragTarget(
//   onAccept: (data) {
//     print("inside onAccept " + data.toString());
//     setState(() {
//       print("inside onAccept " + data.toString());
//       data[0]['data'].add(data);
//     });
//   },
//   onWillAccept: (data) {
//     print("inside onAccept " + data.toString());
//     return true;
//   },
//   builder: (context, accepted, rejected) {
//     print('hello hhhhhhhhhhhh');
//     return DataBox('hello ', 'world');
//   }
// ),
// DragTarget(
//   onAccept: (String data) {
//     print("inside onAccept " + data);
//     // setState(() {
//     //   print("inside onAccept " + data.toString());
//     //   data[0]['data'].add(data);
//     // });
//   },
//   onWillAccept: (String data) {
//     print("inside onAccept " + data);
//     return true;
//   },
//   builder: (context, List<String> accepted, rejected) {
//     print('hello hhhhhhhhhhhh');
//     return DataBox('hello ', 'world');
// return Card(
//     color: Colors.grey[400],
//     child: Container(
//         height: 600,
//         width: MediaQuery.of(context).size.width,
//         child: ListView(shrinkWrap: true, children: <Widget>[
//           Center(
//               child: Padding(
//                   padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
//                   child: Text(
//                     'Test' + ' ',
//                     style: TextStyle(fontSize: 23),
//                   ))),
//           ListView.builder(
//               shrinkWrap: true,
//               physics: ScrollPhysics(),
//               scrollDirection: Axis.vertical,
//               itemCount: data[0]['data'].length,
//               itemBuilder: (BuildContext context, j) {
//                 // print(data.length);
//                 Map d = data[0]['data'][j];
//                 return DataBox(d['title'], d['sub']);
//               })
//         ])));
//   }
// ),

/*data.map((i) {
                  return Builder(builder: (BuildContext context) {
                    return DragTarget(onAccept: (data) {
                      print("inside onAccept " + data.toString());
                      setState(() {
                        print("inside onAccept " + data.toString());
                        data[i]['data'].add(data);
                      });
                    }, onWillAccept: (data) {
                      print("inside onAccept " + data.toString());
                      return true;
                    }, builder: (
                      BuildContext context,
                      List<dynamic> accepted,
                      List<dynamic> rejected,
                    ) {
                      return Card(
                          color: Colors.grey[400],
                          child: Container(
                              height: 600,
                              width: MediaQuery.of(context).size.width,
                              child:
                                  ListView(shrinkWrap: true, children: <Widget>[
                                Center(
                                    child: Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(10, 5, 10, 5),
                                        child: Text(
                                          'Test' + ' ',
                                          style: TextStyle(fontSize: 23),
                                        ))),
                                ListView.builder(
                                    shrinkWrap: true,
                                    physics: ScrollPhysics(),
                                    scrollDirection: Axis.vertical,
                                    itemCount: i['data'].length,
                                    itemBuilder: (BuildContext context, j) {
                                      // print(data.length);
                                      Map d = i['data'][j];
                                      return DataBox(d['title'], d['sub']);
                                    })
                              ])));
                    });
                  });
                }).toList()*/
